﻿using MahApps.Metro.Controls;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.IO;
using System.Net;
using XeniaAssesmentTool.APIModels;

namespace XeniaAssesmentTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public EstimationRequestList EstimationRequestList { get; set; }
        public BundleImageResponse bundleImageResponse;
        public EstimationRequest currentEstimationRequest;

        public int iCurrentGUID = 0;
        private XImage logo;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            logo = XImage.FromFile("./Assets/XeniaSolutionLog.png");
        }

        private void ButtonGenerateReport_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PdfDocument pdf = new PdfDocument();
            pdf.Info.Title = "My First PDF";

            //First Page
            PdfPage pdfPage = pdf.AddPage();
            XGraphics graph = XGraphics.FromPdfPage(pdfPage);

            //Heading
            graph.DrawRectangle(XBrushes.Maroon, 20, 20, pdfPage.Width.Point - 40, 95);

            //House Address
            XFont font = new XFont("Verdana", 20, XFontStyle.Regular);
            graph.DrawString("Precise Aerial Measurement Report", font, XBrushes.White, pdfPage.Width.Point / 2, 50, XStringFormats.Center);

            font = new XFont("Verdana", 14, XFontStyle.Regular);
            graph.DrawString("Prepared for you by XENIA Solutions", font, XBrushes.Gray, pdfPage.Width.Point / 2, 100, XStringFormats.Center);


            //Top view of the house - get image from the server

            string imagePathFirst = "";
            //Insert Top view image
            if (Directory.Exists(iCurrentGUID.ToString()))
            {
                imagePathFirst = Path.Combine(iCurrentGUID.ToString(), @"TopView.jpg");
            }

            XImage image = XImage.FromFile(imagePathFirst);
            //graph.DrawImage(image, 20, 20, 560, 140);
            graph.DrawImage(image, new XRect((pdfPage.Width.Point - 300) / 2, 130, 300, 250));


            //House Address
            if (!String.IsNullOrEmpty(currentEstimationRequest.address))
            {
                font = new XFont("Verdana", 14, XFontStyle.Regular);
                graph.DrawString(currentEstimationRequest.address, font, XBrushes.Black, new XRect(0, 0, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.Center);
            }
            //Company Image
            graph.DrawImage(logo, new XRect((pdfPage.Width.Point - 300) / 2, 450, 300, 75));

            //Company Address
            // font = new XFont("Verdana", 14, XFontStyle.Regular);

            font = new XFont("Verdana", 12, XFontStyle.Regular);
            graph.DrawString("Alpha Summit Construction", font, XBrushes.Black, pdfPage.Width.Point / 2, 550, XStringFormats.Center);
            graph.DrawString("6860 N.Dallas Parkway", font, XBrushes.Black, pdfPage.Width.Point / 2, 575, XStringFormats.Center);
            graph.DrawString("Plano, TX 75024", font, XBrushes.Black, pdfPage.Width.Point / 2, 600, XStringFormats.Center);


            graph.DrawString("Gerry Guevara", font, XBrushes.Gray, pdfPage.Width.Point / 2, 675, XStringFormats.Center);
            graph.DrawString("tel. 817-992-2314", font, XBrushes.Gray, pdfPage.Width.Point / 2, 700, XStringFormats.Center);
            graph.DrawString("email: Gerry@AlphaSummitGC.com", font, XBrushes.Gray, pdfPage.Width.Point / 2, 725, XStringFormats.Center);


            //Second Page
            pdfPage = pdf.AddPage();

            #region ThirdPage
            //------------------Third Page-----------------------------
            PdfPage pdfPage3 = pdf.AddPage();
            graph = XGraphics.FromPdfPage(pdfPage3);

            //Header
            graph.DrawImage(logo, new XRect(20, 10, 200, 50));

            XPen pen = new XPen(XColors.Navy, Math.PI);

            graph.DrawRectangle(XBrushes.Blue, 20, 65, pdfPage3.Width.Point - 40, 25);

            if (!String.IsNullOrEmpty(currentEstimationRequest.address))
            {
                //font = new XFont("Verdana", 14, XFontStyle.Regular);
                //graph.DrawString(currentEstimationRequest.address, font, XBrushes.Black, new XRect(0, 0, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.Center);
                font = new XFont("Verdana", 12, XFontStyle.Regular);
                graph.DrawString(currentEstimationRequest.address, font, XBrushes.White, 25, 80);
            }


            graph.DrawRectangle(XBrushes.LightBlue, 20, 90, pdfPage3.Width.Point - 40, 50);

            font = new XFont("Verdana", 20, XFontStyle.Regular);
            graph.DrawString("IMAGES", font, XBrushes.Blue, 35, 120);

            font = new XFont("Verdana", 10, XFontStyle.Regular);
            graph.DrawString("The following aerial images show different angles of this structure for your reference.", font, XBrushes.Black, 20, 155);

            graph.DrawString("Top View", font, XBrushes.Black, pdfPage.Width.Point / 2, 180, XStringFormats.Center);

            string imagePath = "";
            //Insert Top view image
            if (Directory.Exists(iCurrentGUID.ToString()))
            {
                imagePath = Path.Combine(iCurrentGUID.ToString(), @"TopView.jpg");
            }

            image = XImage.FromFile(imagePath);
            graph.DrawImage(image, new XRect((pdfPage3.Width.Point - 500) / 2, 210, 500, 400));

            //Footer
            graph.DrawRectangle(XBrushes.Blue, 20, pdfPage3.Height.Point - 90, pdfPage3.Width.Point - 40, 7);

            //------------------Third Page-----------------------------
            #endregion

            #region ForthPage
            //------------------Fourth Page-----------------------------
            PdfPage pdfPage4 = pdf.AddPage();
            graph = XGraphics.FromPdfPage(pdfPage4);

            //Header
            graph.DrawImage(logo, new XRect(20, 10, 200, 50));

            graph.DrawRectangle(XBrushes.Blue, 20, 65, pdfPage3.Width.Point - 40, 25);



            if (!String.IsNullOrEmpty(currentEstimationRequest.address))
            {
                //font = new XFont("Verdana", 14, XFontStyle.Regular);
                //graph.DrawString(currentEstimationRequest.address, font, XBrushes.Black, new XRect(0, 0, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.Center);
                font = new XFont("Verdana", 12, XFontStyle.Regular);
                graph.DrawString(currentEstimationRequest.address, font, XBrushes.White, 25, 80);
            }

            graph.DrawRectangle(XBrushes.LightBlue, 20, 90, pdfPage3.Width.Point - 40, 50);

            font = new XFont("Verdana", 20, XFontStyle.Regular);
            graph.DrawString("IMAGES", font, XBrushes.Blue, 35, 120);


            //Page Body
            font = new XFont("Verdana", 10, XFontStyle.Regular);
            graph.DrawString("North Side", font, XBrushes.Black, pdfPage.Width.Point / 2, 180, XStringFormats.Center);

            //Insert Top view image
            if (Directory.Exists(iCurrentGUID.ToString()))
            {
                imagePath = Path.Combine(iCurrentGUID.ToString(), @"NorthSide.jpg");
            }
            image = XImage.FromFile(imagePath);
            graph.DrawImage(image, new XRect((pdfPage4.Width.Point - 250) / 2, 210, 250, 200));

            graph.DrawString("South Side", font, XBrushes.Black, pdfPage.Width.Point / 2, 430, XStringFormats.Center);

            //Insert Top view image
            if (Directory.Exists(iCurrentGUID.ToString()))
            {
                imagePath = Path.Combine(iCurrentGUID.ToString(), @"SouthSide.jpg");
            }
            image = XImage.FromFile(imagePath);
            graph.DrawImage(image, new XRect((pdfPage4.Width.Point - 250) / 2, 460, 250, 200));


            //Footer
            graph.DrawRectangle(XBrushes.Blue, 20, pdfPage3.Height.Point - 90, pdfPage4.Width.Point - 40, 7);

            //------------------Fourth Page-----------------------------
            #endregion


            #region FifthPage
            //------------------Fourth Page-----------------------------
            PdfPage pdfPage5 = pdf.AddPage();
            graph = XGraphics.FromPdfPage(pdfPage5);

            //Header
            graph.DrawImage(logo, new XRect(20, 10, 200, 50));

            graph.DrawRectangle(XBrushes.Blue, 20, 65, pdfPage3.Width.Point - 40, 25);

            if (!String.IsNullOrEmpty(currentEstimationRequest.address))
            {
                //font = new XFont("Verdana", 14, XFontStyle.Regular);
                //graph.DrawString(currentEstimationRequest.address, font, XBrushes.Black, new XRect(0, 0, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.Center);
                font = new XFont("Verdana", 12, XFontStyle.Regular);
                graph.DrawString(currentEstimationRequest.address, font, XBrushes.White, 25, 80);
            }

            graph.DrawRectangle(XBrushes.LightBlue, 20, 90, pdfPage5.Width.Point - 40, 50);

            font = new XFont("Verdana", 20, XFontStyle.Regular);
            graph.DrawString("IMAGES", font, XBrushes.Blue, 35, 120);

            //Page Body
            font = new XFont("Verdana", 10, XFontStyle.Regular);
            graph.DrawString("East Side", font, XBrushes.Black, pdfPage5.Width.Point / 2, 180, XStringFormats.Center);

            //Insert Top view image
            if (Directory.Exists(iCurrentGUID.ToString()))
            {
                imagePath = Path.Combine(iCurrentGUID.ToString(), @"EastSide.jpg");
            }
            image = XImage.FromFile(imagePath);
            graph.DrawImage(image, new XRect((pdfPage5.Width.Point - 250) / 2, 210, 250, 200));

            graph.DrawString("West Side", font, XBrushes.Black, pdfPage5.Width.Point / 2, 430, XStringFormats.Center);

            //Insert Top view image
            if (Directory.Exists(iCurrentGUID.ToString()))
            {
                imagePath = Path.Combine(iCurrentGUID.ToString(), @"WestSide.jpg");
            }
            image = XImage.FromFile(imagePath);
            graph.DrawImage(image, new XRect((pdfPage5.Width.Point - 250) / 2, 460, 250, 200));

            //Footer
            graph.DrawRectangle(XBrushes.Blue, 20, pdfPage5.Height.Point - 90, pdfPage5.Width.Point - 40, 7);

            //------------------Fourth Page-----------------------------
            #endregion


            string pdfFilename = "firstpage.pdf";
            pdf.Save(pdfFilename);
        }

        private void ButtonGetGUID_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            EstimationRequestList = APIService.GetUnprocessedImagesBundleID(3);
            DataGridGuids.ItemsSource = this.EstimationRequestList.Result;
        }

        private void ButtonGetBundleImages_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.DataGridGuids.SelectedItem != null){
                currentEstimationRequest = DataGridGuids.SelectedItem as EstimationRequest;
                this.iCurrentGUID = currentEstimationRequest.gu_id;
                bundleImageResponse = APIService.GetBundleImages(iCurrentGUID);
                DataGridBundleImages.ItemsSource = bundleImageResponse.Result;
            }
        }

        private void ButtonDownloadFile_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            for (int i = 0; i < bundleImageResponse.Result.Length; i++)
            {
                if (!Directory.Exists(iCurrentGUID.ToString()))
                {
                    Directory.CreateDirectory(iCurrentGUID.ToString());
                }

                string imagePath = "";
                string extension1 = "";

                if (bundleImageResponse.Result[i].url_type == "1")
                {

                    imagePath = APIService.imageURL + bundleImageResponse.Result[i].path;
                    extension1 = Path.GetExtension(imagePath);
                }
                else
                {
                    imagePath = bundleImageResponse.Result[i].path;
                    extension1 = @".jpg";
                }

                string imageName = "";

                switch (i)
                {
                    case 0:
                        imageName = @"TopView" + extension1;
                        break;

                    case 1:
                        imageName = @"NorthSide" + extension1;
                        break;
                    case 2:
                        imageName = @"SouthSide" + extension1;
                        break;
                    case 3:
                        imageName = @"EastSide" + extension1;
                        break;
                    case 4:
                        imageName = @"WestSide" + extension1;
                        break;

                }

                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(new Uri(imagePath), Path.Combine(iCurrentGUID.ToString(), imageName));
                }

            }

        }
    }
}
