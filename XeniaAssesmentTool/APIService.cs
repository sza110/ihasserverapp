﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XeniaAssesmentTool.APIModels;

namespace XeniaAssesmentTool
{
    public static class APIService
    {
        public static string baseURL = @"http://124.109.39.22:18089/hailstromapi/public/api";
        public static string imageURL = @"http://124.109.39.22:18089/hailstromapi";


        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestType"> 1 = Unprocessed, 2= processed, 3 = All</param>
        public static EstimationRequestList GetUnprocessedImagesBundleID(int requestType)
        {
            var client = new RestClient(baseURL);
            string strRequest = string.Format(@"user\{0}", requestType);
            var request = new RestRequest(strRequest, Method.GET);
            request.RequestFormat = DataFormat.Json;

            // execute the request
            IRestResponse response = client.Execute(request);

            var estimationReqList = JsonConvert.DeserializeObject<XeniaAssesmentTool.APIModels.EstimationRequestList>(response.Content);

            return estimationReqList;
        }


        public static BundleImageResponse GetBundleImages(int bundleGUID)
        {
            var client = new RestClient(baseURL);
            string strRequest = string.Format(@"image\{0}", bundleGUID);
            var request = new RestRequest(strRequest, Method.GET);
            request.RequestFormat = DataFormat.Json;

            // execute the request
            IRestResponse response = client.Execute(request);

            var bundleImagesList = JsonConvert.DeserializeObject<XeniaAssesmentTool.APIModels.BundleImageResponse>(response.Content);

            return bundleImagesList;
        }
        #endregion
    }
}
