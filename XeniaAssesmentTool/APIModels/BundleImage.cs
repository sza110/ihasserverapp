﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XeniaAssesmentTool.APIModels
{
    [Serializable]
    public class BundleImageResponse
    {
        public string statusCode { get; set; }
        public string statusMessage { get; set; }
        public BundleImage[] Result { get; set; }
    }

    [Serializable]
    public class BundleImage
    {

        public int id { get; set; }
        public int gu_id { get; set; }
        public int parent_id { get; set; }
        public string path { get; set; }
        public string view_type { get; set; }
        public string url_type { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}
