﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XeniaAssesmentTool.APIModels
{

    [Serializable]
    public class EstimationRequestList
    {
        public string statusCode { get; set; }
        public string statusMessage { get; set; }
        public EstimationRequest[] Result { get; set; }
    }

    [Serializable]
    public class EstimationRequest
    {

        public int id { get; set; }
        public int gu_id { get; set; }
        public string email { get; set; }
        public int is_processed { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string address { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }

    }

}
